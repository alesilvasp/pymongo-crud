from flask import jsonify
from app.exceptions.posts_exceptions import MissingKeys
from app.models.posts_models import Post

def list_all_posts():
    posts = Post.list_posts()
    return posts

def new_post(**kwargs):
    post = Post(**kwargs)
    Post.save_post(post.__dict__)
    
    return post.__dict__

def delete_and_return(id):
    deleted_post = Post.delete_post_by_id(id)
    return deleted_post

def list_one_post(id):
    post = Post.list_one_post(id)
    return post

def update_post(id, **kwargs):
    post = Post.list_one_post(id)
    for key, value in kwargs.items():
        if key not in post:
            return key
    update = {"$set": kwargs}
    Post.update_one_post(post, update)
    return post