import itertools
import pymongo
from datetime import datetime

client = pymongo.MongoClient("mongodb://localhost:27017/")

db = client['kenzie']

class Post():
    id = itertools.count(1)

    def __init__(self, **kwargs) -> None:
        self.title = kwargs['title']
        self.author = kwargs['author']
        self.id = next(self.id)
        self.updated_at = datetime.now().strftime('%d/%m/%Y %H:%M')
        self.created_at = datetime.now().strftime('%d/%m/%Y %H:%M')
        self.tags = list(kwargs['tags'])
        self.content = kwargs['content']
     
    @staticmethod   
    def save_post(post):
        return db.posts.insert_one(post)
        
    @staticmethod
    def list_posts():
        posts_list = [post for post in db.posts.find()]
        return posts_list
    
    @staticmethod
    def delete_post_by_id(id):
        deleted = db.posts.find_one_and_delete({'id': id})
        return deleted
    
    @staticmethod
    def list_one_post(id):
        post = db.posts.find_one({"id": id})
        return post
    
    @staticmethod
    def update_one_post(data, update):
        update_time = datetime.now().strftime('%d/%m/%Y %H:%M')
        set_time = {'$set': {'updated_at': update_time}}
        db.posts.update_one(data, update)
        db.posts.update_one(data, set_time)
        return True