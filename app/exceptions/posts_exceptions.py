class MissingKeys(Exception):
    keys = ['author', 'title', 'content', 'tags']

    def __init__(self, key) -> None:
        self.missing_key_message = {
            "Missing key": f"Field '{key}' not found or was typed wrong!"
        }
        self.wrong_key_error = {
            "Wrong key": f"{key}"
        }
        super().__init__(self.missing_key_message, self.wrong_key_error)


class PostNotFound(Exception):
    def __init__(self, id) -> None:
        self.missing_post_message = {
            "Missing post": f'Post id: {id} not found!'
        }

        super().__init__(id)
