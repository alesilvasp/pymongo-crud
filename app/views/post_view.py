from flask import request
from bson import json_util
from json import dumps
from app.controller.posts_controller import list_all_posts, list_one_post, new_post, delete_and_return, update_post
from app.exceptions.posts_exceptions import MissingKeys, PostNotFound


def post_view(app):

    @app.post('/posts')
    def create_post():
        data = request.get_json()
        try:
            for key in MissingKeys.keys:
                if key not in data.keys():
                    raise MissingKeys(key)
            post = new_post(**data)
            return dumps(post, default=json_util.default), 201
        except MissingKeys as err:
            return err.missing_key_message, 404
    
    @app.get('/posts/<int:id>')
    def read_post_by_id(id):
        try:
            post_by_id = list_one_post(id)
            if post_by_id == None:
                raise PostNotFound(id)
            return dumps(list_one_post(id), default=json_util.default), 200
        except PostNotFound as err:
            return err.missing_post_message, 404

    @app.delete('/posts/<int:id>')
    def delete_post(id):
        try:
            deleted = delete_and_return(id)
            if deleted == None:
                raise PostNotFound(id)            
            return dumps(deleted, default=json_util.default), 410
        except PostNotFound as err:
            return err.missing_post_message, 404

    @app.get('/posts')
    def read_posts():
        return dumps(list_all_posts(), default=json_util.default), 200

    @app.patch('/posts/<int:id>')
    def update_posts(id):
        data = request.get_json()
        try:
            post_id = list_one_post(id)
            if post_id == None:
                raise PostNotFound(id)       
            teste = update_post(id, **data)
            print(type(teste))
            if type(teste) == str:
                raise MissingKeys(teste)
            return dumps(list_one_post(id), default=json_util.default), 202
        except PostNotFound as err:
            return err.missing_post_message, 404
        except MissingKeys as err:
            return err.wrong_key_error, 404
